# Inaccessible accessible web

This is a playground to build an inaccessible web with 100% score point in lighthoouse.

Inspired by "[Building the most inaccessible site possible with a perfect Lighthouse score](https://www.matuzo.at/blog/building-the-most-inaccessible-site-possible-with-a-perfect-lighthouse-score/)"

You can see it in the [Gitlab Pages](https://brokarth.gitlab.io/inaccessible-accessible-web/)
