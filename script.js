document.querySelector("body").classList.add("loaded");
document.addEventListener('keydown', e => {
    e.preventDefault();
});

if(!("pointerEvents" in document.body.style)) {
    document.addEventListener('click', e => {
        e.preventDefault();
    });
}
